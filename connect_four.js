//Marcus Bermel & Brandon Bosso

//Draws the board and adds a mouse listener to the canvas
//to update the position of the hovering piece. A separate
//listener will be added to monitor clicks and drop the piece as needed.


function firstDraw() {
window.allowInput =  true;
window.startup = true;
window.playerOne = true;
window.playerOneCanvas = document.getElementById("player_one");
window.playerOneContext = document.getElementById("player_one").getContext("2d");
var canvas = document.getElementById("connect_four");
            var context = canvas.getContext("2d");
              if(startup){
            	context.beginPath();
            
            	
            	context.arc(450, -15, 40, 0, Math.PI, false);
		
      		context.fillStyle = 'green';	
            	context.fill();
		
		}  
    draw();
    
    //Hover listener to update location of current piece
    document.getElementById("connect_four").addEventListener('mousemove', function(evt) {
            var canvas = document.getElementById("connect_four");
            var context = canvas.getContext("2d");
            
            context.beginPath();
            context.rect(0, 0, 700, 30);
            context.fillStyle = '#1B0A2A';
            context.fill();

            	context.beginPath();
            
            	var x = Math.floor(getMouseX(canvas, evt));
            	context.arc(x, -15, 40, 0, Math.PI, false);
		
      		context.fillStyle = playerOne ? 'green' : 'red';	
            	context.fill();

        }, false);
    
    //Click listener to drop current piece and check for a win
    document.getElementById("connect_four").addEventListener('click', function(evt) {
        if(allowInput){
            var x = getMouseX(document.getElementById("connect_four"), evt);
            var column = Math.floor(x/100);
            dropPiece(column);
            draw();
		context.beginPath();
 
             context.arc(x, -15, 40, 0, Math.PI, false);
                 if(playerOne){
                         context.fillStyle = 'green';
                 }else{
                         context.fillStyle = 'red';
                 }
             context.fill();

        }
            
        }, false);
startup = false;
}

// Code for drawing the board
function draw() {
    var canvas = document.getElementById("connect_four");
    var context = canvas.getContext("2d");
    
    context.beginPath();
    context.rect(0, 30, 700, 630);
    context.fillStyle = 'black';
    context.fill();
    
    //Draw the pieces and empty spots on the board
    for(var i=0; i<7; i++){
            for(var j=0; j<6; j++){
                    context.save();
                    context.globalCompositeOperation = 'destination-out';
                    context.beginPath();
                    context.arc((i*100)+50, (j*100)+50 + 30, 40, 0, 2*Math.PI);
                    context.fill();
                    context.restore();
                    
                    context.lineWidth = 3;

                    context.strokeStyle = 'pink';
                    context.stroke();
            }
    }
    var ctx = document.getElementById("player_one").getContext("2d");
    ctx.beginPath();
    //ctx.clearRect(0,30,700,630);
    // ctx.fillStyle = 'white';
    // ctx.fill();
}

//Return the x value of the mouse on hover.
//Used to draw the piece hovering above the board
//waiting to be dropped.
function getMouseX(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return (evt.clientX - rect.left);
}

      function drawGamePiece(context) {
          context.beginPath();
          context.arc(gamePiece.x, gamePiece.y, gamePiece.radius, 0, 2 * Math.PI, false);
          if(gamePiece.turn){
            context.fillStyle = 'green';
            context.strokeStyle = 'green';
          }else{
            context.fillStyle = 'red';
            context.strokeStyle = 'red';
          }

          context.fill();
          context.closePath();
          context.lineWidth = 5;
          context.stroke();
 
      }
      function animate(backCanvas, startTime, endYPos, context) {
        allowInput = false;
        // update
        var time = (new Date()).getTime() - startTime;

        var linearSpeed = 500;
        // pixels / second
        var newY = linearSpeed * time / 1000;
        
        

        if(newY < endYPos ) {
          gamePiece.y = newY;
        }else{
            allowInput = true;
            return;
        }
        context.clearRect(gamePiece.x-60, gamePiece.y-60, 100, 100);


        drawGamePiece(context);

        // request new frame
        requestAnimFrame(function() {
          animate(backCanvas, startTime, endYPos, context);
        });
      }
      
      var pieceCount = [[], [], [], [], [], [], []];

      function dropPiece(column){
                window.requestAnimFrame = (function(callback) {
          return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
          function(callback) {
              window.setTimeout(callback, 1000 / 60);
            };
          })();

          centerX = column*100 + 50;
          window.gamePiece = {
            x: centerX,
            y: 0,
            radius: 45,
            lineWidth: 3,
            turn: playerOne,
          };
            var context = playerOneContext;
            var canvas = playerOneCanvas;
          targetY = 583-(100*pieceCount[column].length);
          var row = Math.floor(targetY/100);
          pieceCount[column].push(playerOne ? "O" : "T");
          drawGamePiece(context);
          var win = checkForWin(column, row);
          if(win){
            console.log("Winner");
            endGame();
          }
          // wait one second before starting animation
                setTimeout(function() {
                  var startTime = (new Date()).getTime();
                  animate(canvas, startTime, targetY, context);
                }, 0);
                playerOne = !playerOne;
                context.save();
      }
      
function endGame() {
        document.getElementById("test").innerHTML = "end";
        document.getElementById("connect_four").classList.remove("fadeIn");
        document.getElementById("connect_four").classList.add("fadeOut");
        document.getElementById("player_one").classList.add("fadeOut");
        
        var canvas = document.getElementById("win_screen");
        var context = canvas.getContext('2d');
        
        context.beginPath();
        context.rect(0, 0, 700, 630);
        context.fillStyle = 'white';
        context.fill();
        
        context.font="40px Century Gothic";
        context.fillStyle = 'black';
        context.fillText("You Win!", 300, 300);
        
        document.getElementById("win_screen").classList.add("fadeIn");
}

function checkForWin(column, row){
    console.log("New turn!\n\n\n\n\n\n");
    var playerToCheck = playerOne ? "O" : "T";
    var chain = 0;
    var win = false;
    // Check columns for win
    for(var j = 0; j < 5; j++){
        var piece = pieceCount[column][j];
        if(piece != playerToCheck){
            chain = 0;
            continue;
        }
        chain++;
        if(chain >= 4){
            return true;
        }
    }
    chain = 0;
    
    // Check rows for win.
    for(var i = 0; i < 6; i++){
        var piece = pieceCount[i][5-row];
        if(piece != playerToCheck){
            if(i == column){
                chain++;
                if(chain >= 4){
                    return true;
                }
                continue;
            }
            chain = 0;
            continue;
        }
        chain++;
        if(chain >= 4){
            return true;
        }
    }

    chain = 0;
    upperLeft = false;
    upperRight = false;
    lowerLeft = false;
    lowerRight = false;

    if(column - 1 >= 0 && row - 1 >= 0 && pieceCount[column-1][row-1] == playerToCheck){
        upperLeft = true;
    }
    if(column + 1 <= 6 && (row - 1) >= 0 && pieceCount[column+1][5-(row-1)] == playerToCheck){
        upperRight = true;
    }
    if(column - 1 >= 0 && row + 1 <= 5){
        var nuts = pieceCount[column-1][row+1];
        console.log(row);
        if(pieceCount[column-1][5-(row+1)] == playerToCheck){
            lowerLeft = true;
        }
    }
    if(column + 1 <= 6 && row + 1 <= 5 && pieceCount[column+1][5-(row+1)] == playerToCheck){
        lowerRight = true;
    }
    upperLeftTotal = upperRightTotal = lowerLeftTotal = lowerRightTotal = 0;
    chain = 0;
    if(upperLeft){
        // Add one to the chain because we know it exists, so we don't have to 
        // re-process it.
        upperLeftTotal++;
        xIndex = column-1;
        yIndex = row-1;
        while(xIndex >= 0){
            if(pieceCount[xIndex][yIndex] == playerToCheck){
                upperLeftTotal++;
                xIndex--;
                yIndex--;
            }
            else{break;}
        }
        if(upperLeftTotal >= 4){
            return true;
        }
    }
    if(lowerLeft){
        // Add one to the chain because we know it exists, so we don't have to 
        // re-process it.
        var lowerLeftTotal = 1;
        xIndex = column-1;
        yIndex = 5-(row+1);
        while(xIndex >= 0){
            if(pieceCount[xIndex][yIndex] == playerToCheck){
                lowerLeftTotal++;
                xIndex--;
                yIndex++;
            }
            else{break;}
        }
        if(lowerLeft && upperRight){
            lowerLeftTotal = upperLeftTotal+lowerLeftTotal;
        }else{
            lowerLeftTotal = lowerLeft ? lowerLeftTotal : upperLeftTotal;
        }
        if(lowerLeftTotal >= 4){
            return true;
        }
    }
    if(upperRight){
        // Add one to the chain because we know it exists, so we don't have to 
        // re-process it.
        upperRightTotal++;
        xIndex = (column == 6) ? 6 : column+1;
        yIndex = 5-(row-1);
        while(xIndex <= 6){
            if(pieceCount[xIndex][yIndex] == playerToCheck){
                upperRightTotal++;
                xIndex++
                yIndex++;
            }
            else{break;}
        }
        if(upperRightTotal >= 4){
            return true;
        }
    }
    if(lowerRight){
        // Add one to the chain because we know it exists, so we don't have to 
        // re-process it.
        lowerRightTotal++;
        xIndex = column-1;
        yIndex = 5-(row+1);
        while(xIndex >= 0){
            if(pieceCount[xIndex][yIndex] == playerToCheck){
                lowerRightTotal++;
                xIndex--;
                yIndex--;
            }
            else{break;}
        }
        if(lowerRight && upperLeft){
            lowerRightTotal = upperRightTotal+lowerRightTotal;
        }else{
            lowerRightTotal = lowerLeft ? lowerRightTotal : upperRightTotal;
        }
        if(lowerRightTotal >= 4){
            return true;
        }
    }    return false;
}
