//Brandon Bosso & Marcus Bermel

// begin code for animation

var canvas=document.getElementById("canvas"),
ctx = canvas.getContext("2d");

canvas.width = 700;
canvas.height = 630;
// Will be passed into the javascript file: targetX, targetY, x, y
var targetX = 50,
    targetY = 150,
    x = 10,
    y = 10,
    velX = 0,
    velY = 0,
    speed = 5;

function update(){
    var tx = targetX - x,
        ty = targetY - y,
        dist = Math.sqrt(tx*tx+ty*ty),
        rad = Math.atan2(ty,tx),
        angle = rad/Math.PI * 180;

        velX = (tx/dist)*speed,
        velY = (ty/dist)*speed;
    
        x += velX
        y += velY
            
        ctx.clearRect(0,0,500,500);
        ctx.beginPath();
        ctx.arc(x,y,5,0,Math.PI*2);
        ctx.fill();
    
    setTimeout(update,10);
}

update();


// end code